﻿using System;

namespace Lesson1
{
    class DZ1
    {
        public void First()
        {
            for (int i = 0; i <= 150; i++)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                }
            }
        }

        public void Second()
        {
            for (int i = 50; i <= 200; i++)
            {
                if (i % 2 > 0)
                {
                    Console.WriteLine(i);
                }
            }
        }

        public void Third()
        {
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            int p = -1;
            do
            {
                p++;
                if (arrTemp[p] == 65)
                {
                    Console.WriteLine("Position of Number is {0}", p + 1);
                    break;
                }
            }
            while (p < arrTemp.Length - 1);
        }

        public void Four()
        {
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            foreach (var i in arrTemp)
            {
                Console.WriteLine(i * 2);
            }
        }
    }
}
